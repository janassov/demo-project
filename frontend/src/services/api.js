import axios from 'axios'; 
const baseURL = process.env.VUE_APP_API_URL; // Базовый URL API 
// Функция для получения данных с API 
export const getRestaurants = () => { 
    return axios.get(baseURL + '/restaurants');
}; 
